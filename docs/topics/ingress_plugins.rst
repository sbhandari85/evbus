INGRESS PLUGINS
===============

This is a pseudocode example of how to write an ingress plugin:

my_project/ingress/my_plugin.py

.. code-block:: python

    # Import an async message queue library
    import mq_lib


    def __init__(hub):
        # Identify to evbus which acct providers can be used with your plugin
        hub.ingress.my_plugin.ACCT = ["my_acct_provider"]


    # This function must be called "publish", be async and have exactly these options
    async def publish(hub, ctx, body: bytes):
        """
        This is an example of what an acct profile looks like in order to use this publish plugin:

        .. code-block:: sls

            my_acct_provider:
              my_profile:
                host: localhost
                port: 12345
                username: root
                password: password!
        """
        # Use the data stored in ctx.acct to connect to your message queue
        connection = mq_lib.connect(
            host=ctx.acct.host,
            port=ctx.acct.port,
            username=ctx.acct.username,
            password=ctx.acct.password,
        )

        # Use the routing_key to get a specific channel from your message queue library as appropriate
        channel = connection.get_channel()

        # The message body has already been serialized, go ahead and publish it to the ingress queue
        await channel.put(body)


Your project needs to have a `conf.py` that implements `evbus` cli options as needed and the `ingress` dynamic namespace.

my_project/conf.py

.. code-block:: python

    CLI_CONFIG = {
        "serial_plugin": {"source": "evbus"},
    }

    DYNE = {"ingress": ["ingress"]}
