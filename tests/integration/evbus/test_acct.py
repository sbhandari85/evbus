import tempfile


async def test_profiles(hub):
    profile = {"host": None, "port": 1234}
    data = {
        "pika": {
            # Host is None so that even if there is an active local rabbitmq server we don't connect
            "ingress_test_profile": profile,
        }
    }
    with tempfile.NamedTemporaryFile(delete=True) as fh:
        key = hub.crypto.fernet.generate_key()
        enc = hub.crypto.fernet.encrypt(data, key)
        fh.write(enc)
        fh.flush()

        contexts = await hub.evbus.acct.profiles(
            acct_file=fh.name,
            acct_key=key,
        )

    assert "pika" in contexts
    assert "ingress_test_profile" in contexts["pika"][0]

    assert contexts["pika"][0]["ingress_test_profile"] == profile


async def test_duplicate_profiles(hub):
    profile = {"host": None, "port": 1234}
    data = {
        "pika": [
            # Host is None so that even if there is an active local rabbitmq server we don't connect
            {"ingress_test_profile": profile},
            {"ingress_test_profile": profile},
        ]
    }
    with tempfile.NamedTemporaryFile(delete=True) as fh:
        key = hub.crypto.fernet.generate_key()
        enc = hub.crypto.fernet.encrypt(data, key)
        fh.write(enc)
        fh.flush()

        contexts = await hub.evbus.acct.profiles(
            acct_file=fh.name,
            acct_key=key,
        )

    assert "pika" in contexts
    assert "ingress_test_profile" in contexts["pika"][0]
    assert "ingress_test_profile" in contexts["pika"][1]

    assert contexts["pika"][0]["ingress_test_profile"] == profile
    assert contexts["pika"][1]["ingress_test_profile"] == profile


async def test_empty_none(hub):
    """
    Verify no profiles when None is  passed
    """
    assert not await hub.evbus.acct.profiles(
        acct_file=None,
        acct_key=None,
        acct_data=None,
    )


async def test_empty(hub):
    """
    Verify no profiles when empty objects used as args
    """
    assert not await hub.evbus.acct.profiles(
        acct_file="",
        acct_key="",
        acct_data={},
    )


async def test_defaults(hub):
    """
    Verify that the evbus.acct.profiles can be called with no arguments
    """
    assert not await hub.evbus.acct.profiles()


async def test_kwargs(hub, mock_hub):
    """
    Verify that kwargs are forwarded
    """
    hub.evbus.acct.profiles = mock_hub.evbus.acct.profiles
    await hub.evbus.acct.profiles(acct_file="f", a=1, b=2, c=3)
    mock_hub.evbus.acct.profiles.assert_called_once_with(acct_file="f", a=1, b=2, c=3)
