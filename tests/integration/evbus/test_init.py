async def test_start(hub):
    await hub.evbus.broker.init()
    hub.evbus.BUS.put_nowait(hub.evbus.init.STOP_ITERATION)
    await hub.evbus.init.start({})


async def test_stop(hub, event_loop):
    t = event_loop.create_task(hub.evbus.init.start({}))
    await hub.pop.loop.sleep(0)
    await hub.evbus.init.stop()
    await t


async def test_join(hub, event_loop):
    await hub.evbus.broker.init()
    hub.evbus.BUS.put_nowait(hub.evbus.init.STOP_ITERATION)
    t = event_loop.create_task(hub.evbus.init.join())
    hub.evbus.RUN_FOREVER = False
    await hub.evbus.init.start({})
    await t
