import asyncio
import queue

import evbus.evbus.broker as broker


async def test_init_synchronous(hub):
    assert isinstance(hub.evbus.BUS, queue.Queue)
    bus = await hub.evbus.broker.init()
    assert isinstance(bus, asyncio.Queue)


async def test_init_asynchronous(hub):
    bus = await hub.evbus.broker.init()
    assert isinstance(bus, asyncio.Queue)


async def test_put(hub):
    await hub.evbus.broker.put("body")
    event = await hub.evbus.broker.get()
    assert event.body == b'"body"'


async def test_put_nowait(hub):
    hub.evbus.broker.put_nowait("body")
    event = await hub.evbus.broker.get()
    assert event.body == b'"body"'


async def test_propagate(hub):
    contexts = {"internal": [{"default": {"routing_key": "key"}}]}
    event = broker.Event(profile="default", body="body")
    await hub.evbus.broker.propagate(contexts, event)
    body = await hub.ingress.internal.QUEUE["key"].get()
    assert body == "body"
