CLI_CONFIG = {
    "serial_plugin": {},
}
CONFIG = {
    "serial_plugin": {
        "help": "The plugin used to serialize data. Defaults to json",
        "default": "json",
    },
}
DYNE = {
    "log": ["log"],
    "evbus": ["evbus"],
    "ingress": ["ingress"],
    "match": ["match"],
}
